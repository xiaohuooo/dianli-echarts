import Vue from 'vue'
import VueRouter from 'vue-router'
import DataVisualization from '../views/DataVisualization.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: DataVisualization
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
